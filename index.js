const express = require("express");
const app = express();
app.use(express.json());
const baseConsulta = {};
const funcoes = {
    IngressoCriado: (ingresso) => {
        baseConsulta[ingresso.contador] = ingresso;
    },
    IngressoCriado: (ingresso) => {
        const ingressos =
            baseConsulta[ingresso.clienteId]["ingressos"] || [];
        ingressos.push(ingresso);
        baseConsulta[ingresso.clienteId]["ingressos"] =
            ingressos;
    },
    IngressoAtualizado: (ingresso) => {
        const ingressos =
            baseConsulta[ingresso.ingressoId]["ingressos"];
        const indice = ingressos.findIndex((o) => o.id ===
            ingresso.id);
        ingressos[indice] = ingresso;
    },
};

app.get("/lembretes", (req, res) => {
    res.status(200).send(baseConsulta);
});
app.post("/eventos", (req, res) => {
    try {
        funcoes[req.body.tipo](req.body.dados);
    } catch (err) {}
    res.status(200).send(baseConsulta);
});
app.listen(7000, () => console.log("Consultas. Porta 7000"));